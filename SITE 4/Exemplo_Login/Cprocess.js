const {createApp} = Vue;

createApp({
    data(){
        return{
            display:"",
            historico: "",
            power: false,
            ligado: false,
            operador: null,
            numero_atual: null,
            numero_anterior: null,
        };
    },

    methods:{
        numero(valor){
            if (this.power == 1)
            {
                if (this.display == "0")
                {
                    this.display = valor.toString();
                }
                else
                {
                    if (this.operador == "=")
                    {
                        this.display = "";
                        this.operador = null;
                    }
                    this.display += valor.toString();
                }
            }
        },

        btclear(){
            if (this.power)
            {
                this.display = "0";
                this.numero_atual = null;
                this.numero_anterior = null;
                this.operador = null;
            }
        },

        btpower(val){
            this.power = val;
            if (!this.power)
            {
                this.display = "";
                this.ligado = false;
                this.historico = "";
            }
            if (this.power)
            {
                if (!this.ligado)
                {
                    this.display = "0";
                }
                this.ligado = true;
                this.numero_atual = null;
                this.numero_anterior = null;
                this.operador = null;
            }
        },

        decimal(){
            if (this.power)
            {
                if (!this.display.includes("."))
                {
                    if (this.operador == "=")
                    {
                        this.display = "0" + "."
                        this.operador = null;
                    }
                    else
                    {
                        this.display += ".";
                    }
                }
            }
        },

        operacoes(operacao){
            if (this.power)
            {
                if (this.operador != null)
                {
                    const display_atual = parseFloat(this.display);

                    switch (this.operador)
                    {
                        case "+":
                            this.display = ((this.numero_atual + display_atual).toFixed(8)) * 1;
                            this.historico += " " + this.display;
                        break;
                        case "-":
                            this.display = ((this.numero_atual - display_atual).toFixed(8)) * 1;
                            this.historico += " " + this.display;
                        break;
                        case "*":
                            this.display = ((this.numero_atual * display_atual).toFixed(8)) * 1;
                            this.historico += " " + this.display;
                        break;
                        case "/":
                            this.display = ((this.numero_atual / display_atual).toFixed(8)) * 1;
                            if (!this.display == "NaN" || !this.display == "Infinity")
                            {
                                this.historico += " " + this.display;
                            }
                        break;
                    }

                    this.numero_anterior = this.numero_atual;
                    this.numero_atual = null;
                    this.operador = null;
                }
                
                if (operacao != "=")
                {
                    this.operador = operacao;
                    this.numero_atual = parseFloat(this.display);
                    this.display = "0";
                }
                else
                {
                    this.operador = operacao;
                    if (this.display == "Infinity" || this.display == "Infinito") 
                    {
                        this.display = "Infinito";
                    }

                    else if (isNaN(this.display))
                    {
                        this.display = "Indefinido";
                    }

                    /* if(this.display.includes(".0000000000000000"))
                    {
                        const display_atual = parseFloat(this.display);
                        this.display = (display_atual.toFixed(0)).toString();
                    } */
                }

                /*alert("O numero atual é: " + this.numero_atual);
                alert("O numero atual é: " + this.operador);*/
            }
        },

    },

}).mount("#app");