const {createApp} = Vue;

createApp({
    data(){
        return{
            username: "",
            password: "",
            animacao: null,
            error: null,
            sucesso: null,
            user_admin: false,
            mostrar_entrada: false,
            calculadora: false,

            usernameCad: "",
            passwordCad: "",
            passwordCad2: "",

            usuarios: ["1", "felipe", "marcio"],
            senhas: ["1", "2", "3"],

            mostrar_lista: false,
        };
    },

    methods:{
        // login()
        // {
        //     setTimeout(() => 
        //     {
        //         if((this.username === "Felipe" && this.password === "senha") ||
        //         (this.username === "Marcio" && this.password === "2") || 
        //         (this.username === "Admin" && this.password === "3"))
        //         {
        //             this.sucesso = "Login efetuado com sucesso! ✌";
        //             this.error = null;

        //             if(this.username === "Admin")
        //             {
        //                 this.user_admin = true;
        //             }
        //         }
        //         else
        //         {
        //             this.error = "Nome de Usuário ou Senha incorreta 😢";
        //             this.sucesso = null;
        //             this.user_admin = false;
        //         }
        //     }, 1000); // <-"delay"
            
        // },

        login2()
        {
            this.error = null;
            this.sucesso = null;
            this.user_admin = false;
            this.mostrar_entrada = false;

            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas"))
            {
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }

            this.animacao = ".";
            setTimeout(() => 
            {
                this.animacao = "..";
            }, 333);
            setTimeout(() => 
            {
                this.animacao = "...";
            }, 667);

            setTimeout(() => 
            {
                const index = this.usuarios.indexOf(this.username);
                if (index !== -1 && this.senhas[index] === this.password)
                {
                    this.error = null;
                    this.calculadora = true;
                    this.sucesso = "Login efetuado com sucesso! ✌";

                    if(index == 0)
                    {
                        this.user_admin = true;
                        this.sucesso = "Logado com a conta de ADMIN! 🤙";
                    }
                }
                else
                {
                    this.error = "Nome de Usuário ou Senha incorreta 😢";
                    this.username = "";
                    this.password = "";
                    this.sucesso = null;
                    this.user_admin = false;
                    this.calculadora = false;
                }
                
                this.animacao = null;
            }, 1000);

            setTimeout(() => {
                this.mostrar_entrada = true;
            }, 4000);

            setTimeout(() => {
                this.sucesso = null;
                this.error = null;
            }, 5400);
        },
        
        pagina_cadastro()
        {
            this.mostrar_entrada = false;

            if(this.user_admin)
            {
                localStorage.setItem("username", this.username);
                localStorage.setItem("password", this.password);
                
                this.sucesso = "Abilitando preferencias de ADM";

                setTimeout(() => 
                {
                    this.sucesso = "Abilitando preferencias de ADM.";
                }, 333);

                setTimeout(() => 
                {
                    this.sucesso = "Abilitando preferencias de ADM..";
                }, 667);

                setTimeout(() => 
                {
                    this.sucesso = "Abilitando preferencias de ADM...";
                }, 1000);

                setTimeout(() => 
                {
                    window.location.href = "pagina_cadastro.html";
                    this.username = "";
                    this.password = "";
                }, 1333);
            }
        },

        pagina_calculadora()
        {
            localStorage.setItem("username", this.username);
            localStorage.setItem("password", this.password);
            
            this.mostrar_entrada = false;
            
            this.sucesso = "Carregando a Super calculadora";

            setTimeout(() => 
            {
                this.sucesso = "Carregando a Super calculadora.";
            }, 333);

            setTimeout(() => 
            {
                this.sucesso = "Carregando a Super calculadora..";
            }, 667);

            setTimeout(() => 
            {
                this.sucesso = "Carregando a Super calculadora...";
            }, 1000);

            setTimeout(() => 
            {
                window.location.href = "calculadora.html";
                this.username = "";
                this.password = "";
            }, 1333);
        },

        adicionar_usuario()
        {
            this.username = localStorage.getItem("username");
            this.password = localStorage.getItem("password");

            this.mostrar_entrada = false;

            if (this.username === "1")
            {
                this.error = null;
                this.sucesso = null;

                this.sucesso = "Cadastrando uma conta no sistema";

                setTimeout(() => 
                {
                    this.sucesso = "Cadastrando uma conta no sistema.";
                }, 333);

                setTimeout(() => 
                {
                    this.sucesso = "Cadastrando uma conta no sistema..";
                }, 667);

                setTimeout(() => 
                {
                    this.sucesso = "Cadastrando uma conta no sistema...";

                    if (this.usernameCad == "")
                    {
                        setTimeout(() => {
                            this.sucesso = null;
                            this.error = "Campo de Usuario não preenchido";

                            setTimeout(() => {
                                this.mostrar_entrada = true;
                            }, 1200);
                        }, 500);
                    }
                    else
                    {
                        if (this.usernameCad.includes(" "))
                        {
                            setTimeout(() => {
                                this.sucesso = null;
                                this.error = "Nome não pode conter espaço🚫";
                                
                                setTimeout(() => {
                                    this.mostrar_entrada = true;
                                }, 1200);
                            }, 500);
                        }
                        else
                        {
                            if (this.usuarios.includes(this.usernameCad))
                            {
                                setTimeout(() => {
                                    this.sucesso = null;
                                    this.error = "Nome de Ususario ja utilizado";
                                    
                                    setTimeout(() => {
                                        this.mostrar_entrada = true;
                                    }, 1200);
                                }, 500);
                            }
                            else
                            {
                                if (!(this.passwordCad && this.passwordCad === this.passwordCad2))
                                {
                                    setTimeout(() => {
                                        this.sucesso = null;
                                        this.error = "Senha Incorreta!!!";
                                        
                                        setTimeout(() => {
                                            this.mostrar_entrada = true;
                                        }, 1200);
                                    }, 500);
                                }
                                else
                                {
                                    if (this.passwordCad.includes(" "))
                                    {
                                        setTimeout(() => {
                                            this.sucesso = null;
                                            this.error = "A senha não pode conter espaço🚫";
                                            
                                            setTimeout(() => {
                                                this.mostrar_entrada = true;
                                            }, 1200);
                                        }, 500);
                                    }
                                    else
                                    {
                                        this.usuarios.push(this.usernameCad);
                                        this.senhas.push(this.passwordCad);

                                        localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                                        localStorage.setItem("senhas", JSON.stringify(this.senhas));

                                        setTimeout(() => 
                                        {
                                            this.error = null;
                                            this.sucesso = "🌟Conta cadastrada🌟";
                            
                                            setTimeout(() => {
                                                this.mostrar_entrada = true;
                            
                                                setTimeout(() => {
                                                    this.sucesso = null;
                                                    this.error = null;
                                                }, 1200);
                                            }, 1200);
                                        }, 500);
                                    }

                                    this.usernameCad = "";
                                    this.passwordCad = "";
                                    this.passwordCad2 = "";
                                }
                            }
                        }
                    }
                }, 1000);
            }
        },

        ver_cadastrados()
        {
            this.username = localStorage.getItem("username");
            this.password = localStorage.getItem("password");

            if (this.username === "1")
            {
                this.mostrar_lista =! this.mostrar_lista;
                
                if (localStorage.getItem("usuarios") && localStorage.getItem("senhas"))
                {
                    this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                    this.senhas = JSON.parse(localStorage.getItem("senhas"));
                }
            }
        },

        excluir_usuarios(usuario)
        {
            this.mostrar_entrada = false;

            if (usuario === "1")
            {
                setTimeout(() => {
                    this.sucesso = null;
                    this.error = "O Usuário admin não pode ser excluido✋✋✋";

                    setTimeout(() => {
                        this.mostrar_entrada = true;

                        setTimeout(() => {
                            this.error = null;
                        }, 1200);
                    }, 1200);
                }, 500);
                return;
            }

            if(confirm("Tem certeza que deseja excluir esta conta?"))
            {
                const index = this.usuarios.indexOf(usuario);
                if (index !== -1)
                {
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);

                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));
                }
            }
        },
    },

}).mount("#app");