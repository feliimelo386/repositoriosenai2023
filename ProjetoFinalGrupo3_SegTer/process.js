const {createApp} = Vue;

createApp({
    data(){
        return{
            username: "",
            password: "",
            user_email: "",

            animacao: null,
            error: null,
            sucesso: null,

            user_admin: false,
            mostrar_entrada: false,
            logado: false,

            tela_login: false,
            fazer_login: false,
            criar_conta: false,

            mostrar_lista: false,


            usernameCad: "",
            emailCad: "",
            passwordCad: "",
            passwordCad2: "",

            names: ["Admin", "conta teste"],
            emails: ["1", "2"],
            senhas: ["1", "2"],

            products: [
                {
                    id: 1,
                    name: "CocaCola de Garrafa 2L",
                    description: "",
                    price: 8.99,
                    image: "./midias/CocaCola_garrafa.png"
                },

                {
                    id: 2,
                    name: "CocaCola de Latinha 350ml",
                    description: "",
                    price: 4.99,
                    image: "./midias/CocaCola_latinha.png"
                },

                {
                    id: 3,
                    name: "CocaCola de Garrafinha de Vidro 250ml",
                    description: "",
                    price: 2.99,
                    image: "./midias/CocaCola_garrafinhadevidro.png"
                },

                {
                    id: 4,
                    name: "CocaCola Sem Açucar de Latinha 350ml",
                    description: "",
                    price: 4.99,
                    image: "./midias/CocaColaSemAcucar_latinha.png"
                },

                {
                    id: 5,
                    name: "CocaCola Zero de Latinha 350ml",
                    description: "",
                    price: 4.99,
                    image: "./midias/CocaColaZero_latinha.png"
                },

                {
                    id: 6,
                    name: "Del Valle Sabor Manga 290ml",
                    description: "",
                    price: 3.59,
                    image: "./midias/DelValleManga_latinha.png"
                },

                {
                    id: 7,
                    name: "Del Valle Sabor Maracuja 290ml",
                    description: "",
                    price: 3.59,
                    image: "./midias/DelValleMaracuja_latinha.png"
                },

                {
                    id: 8,
                    name: "Del Valle Sabor Pessego 290ml",
                    description: "",
                    price: 3.59,
                    image: "./midias/DelVallePessego_latinha.png"
                },

                {
                    id: 9,
                    name: "Fanta Laranja de Garrafa 2L",
                    description: "",
                    price: 6.99,
                    image: "./midias/Fanta_garrafa.png"
                },

                {
                    id: 10,
                    name: "Fanta Laranja de Latinha 350ml",
                    description: "",
                    price: 4.49,
                    image: "./midias/Fanta_latinha.png"
                },
                
                {
                    id: 11,
                    name: "Fanta Guarana de latinha 350ml",
                    description: "",
                    price: 4.49,
                    image: "./midias/FantaGuarana_latinha.png"
                },

                {
                    id: 12,
                    name: "Fanta Uva de Garrafa 2L",
                    description: "",
                    price: 6.99,
                    image: "./midias/FantaUva_garrafa.png"
                },

                {
                    id: 13,
                    name: "Fanta Uva de Latinha 350ml",
                    description: "",
                    price: 4.49,
                    image: "./midias/FantaUva_Latinha.png"
                },

                {
                    id: 14,
                    name: "Guarana Antarctica 2L",
                    description: "",
                    price: 7.99,
                    image: "./midias/GuaranaAntarctica_garrafa.png"
                },

                {
                    id: 15,
                    name: "Garana Antartica de Latinha 350ml",
                    description: "",
                    price: 4.49,
                    image: "./midias/GuaranaAntarctica_latinha.png"
                },

                {
                    id: 16,
                    name: "Guarana Jesus de Garrafa 2L",
                    description: "",
                    price: 9.99,
                    image: "./midias/GuaranaJesus_garrafa.png"
                },

                {
                    id: 17,
                    name: "Guarana Jesus de Latinha 310ml",
                    description: "",
                    price: 3.99,
                    image: "./midias/GuaranaJesus_latinha.png"
                },

                {
                    id: 18,
                    name: "Kuat de Garrafa 2L",
                    description: "",
                    price: 5.99,
                    image: "./midias/Kuat_garrafa.png"
                },

                {
                    id: 19,
                    name: "Kuat de Latinha 350ml",
                    description: "",
                    price: 3.99,
                    image: "./midias/Kuat_latinha.png"
                },

                {
                    id: 20,
                    name: "Schweppes de Garrafa 2L",
                    description: "",
                    price: 4.99,
                    image: "./midias/Schweppes_garrafa.png"
                },

                {
                    id: 21,
                    name: "Schweppes de Latinha 350ml",
                    description: "",
                    price: 2.99,
                    image: "./midias/Schweppes_latinha.png"
                },

                {
                    id: 22,
                    name: "Sprite de Garrafa 2L",
                    description: "",
                    price: 6.99,
                    image: "./midias/Sprite_garrafa.png"
                },

                {
                    id: 23,
                    name: "Sprite de Latinha 350ml",
                    description: "",
                    price: 4.49,
                    image: "./midias/Sprite_latinha.png"
                },

                {
                    id: 24,
                    name: "Sprite Sem Açucar de Latinha 350ml",
                    description: "",
                    price: 4.49,
                    image: "./midias/SpriteSemAcucar_latinha.png"
                },
            ],

            currentProduct: {},
            cart: [],
        };
    },

    mounted(){
        window.addEventListener("hashchange", this.updateProduct);
        this.updateProduct();
    },

    computed:{
        cartItemCount()
        {
            return this.cart.length;
        },

        cartTotal()
        {
            return this.cart.reduce((total, product) => total + product.price, 0);
        },
    },

    methods:{
        finalizar_compra()
        {

        },

        updateProduct()
        {
            const productId = window.location.hash.split("/")[2];
            const product = this.products.find(item => item.id.toString() === productId);
            this.currentProduct = product ? product : {};
        },

        addToCart(product)
        {
            this.cart.push(product);
        },

        removeFroCart(product)
        {
            const index = this.cart.indexOf(product);

            if (index != -1)
            {
                this.cart.splice(index, 1);
            }
        },

        botao_volta_inicio()
        {
            this.fazer_login = false;
            this.tela_login = false;
            this.criar_conta = false;
            this.carrinho = false;
        },

        botao_login()
        {
            this.fazer_login =! this.fazer_login;
            this.criar_conta = false;
            this.tela_login = true;
            this.user_email = "";
            this.password = "";
        },

        botao_criar_conta()
        {
            this.criar_conta =! this.criar_conta;
            this.fazer_login = false;
            this.tela_login = true;
            this.usernameCad = "";
            this.emailCad = "";
            this.passwordCad = "";
            this.passwordCad2 = "";
        },

        login()
        {
            this.error = null;
            this.sucesso = null;
            this.user_admin = false;
            this.mostrar_entrada = false;

            if(localStorage.getItem("emails") && localStorage.getItem("senhas"))
            {
                this.emails = JSON.parse(localStorage.getItem("emails"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }

            this.animacao = ".";
            
            setTimeout(() => 
            {
                this.animacao = "..";

                setTimeout(() => 
                {
                    this.animacao = "...";

                    setTimeout(() => 
                    {
                        const index = this.emails.indexOf(this.user_email);
                        if (index !== -1 && this.senhas[index] === this.password)
                        {
                            this.error = null;
                            this.animacao = null;
                            this.sucesso = "Login efetuado com sucesso! ✌";
                            this.logado = true;

                            this.username = this.names[index];
                            
                            if(index == 0)
                            {
                                this.user_admin = true;
                                this.sucesso = "Logado com a conta de ADMIN! 🤙";
                            }
                            setTimeout(() => {
                                this.email = "";
                                this.password = "";
                                this.tela_login = false;
                                this.fazer_login = false;
                                this.criar_conta = false;
                            }, 1500);
                        }
                        else
                        {
                            this.error = "Email ou Senha incorreta 😢";
                            this.user_email = "";
                            this.password = "";
                            this.sucesso = null;
                            this.user_admin = false; 
                            this.calculadora = false;
                        }
                        
                        this.animacao = null;

                        setTimeout(() => {
                            this.mostrar_entrada = true;

                            setTimeout(() => {
                                this.sucesso = null;
                                this.error = null;
                            }, 1400);
                        }, 2000);
                    }, 200);
                }, 200);
            }, 200);
        },

        criarConta()
        {
            this.username = localStorage.getItem("username");
            this.user_email = localStorage.getItem("user_email");
            this.password = localStorage.getItem("password");

            this.mostrar_entrada = false;

            this.error = null;
            this.sucesso = "Criando uma conta";

            setTimeout(() => 
            {
                this.sucesso = "Criando uma conta.";

                setTimeout(() => 
                {
                    this.sucesso = "Criando uma conta..";

                    setTimeout(() => 
                    {
                        this.sucesso = "Criando uma conta...";

                        if (this.usernameCad == "")
                        {
                            setTimeout(() => {
                                this.sucesso = null;
                                this.error = "Nome de usuario não preenchido";
        
                                setTimeout(() => {
                                    this.mostrar_entrada = true;
                                }, 1200);
                            }, 200);
                        }
                        else
                        {
                            if (!(this.usernameCad.length > 2))
                            {
                                setTimeout(() => {
                                    this.sucesso = null;
                                    this.error = "O nome deve conter pelo menos 3 caracteres";
            
                                    setTimeout(() => {
                                        this.mostrar_entrada = true;
                                    }, 1200);
                                }, 200);
                            }
                            else
                            {
                                if (this.names.includes(this.usernameCad))
                                {
                                    setTimeout(() => {
                                        this.sucesso = null;
                                        this.error = "Nome de usuario já utilizado";
                
                                        setTimeout(() => {
                                            this.mostrar_entrada = true;
                                        }, 1200);
                                    }, 200);
                                }
                                else
                                {
                                    if (this.emailCad == "")
                                    {
                                        setTimeout(() => {
                                            this.sucesso = null;
                                            this.error = "Campo do email não preenchido";
                    
                                            setTimeout(() => {
                                                this.mostrar_entrada = true;
                                            }, 1200);
                                        }, 200);
                                    }
                                    else
                                    {
                                        if (this.emailCad.includes(" "))
                                        {
                                            setTimeout(() => {
                                                this.sucesso = null;
                                                this.error = "O email não pode conter espaço🚫";
                                                    
                                                setTimeout(() => {
                                                    this.mostrar_entrada = true;
                                                }, 1200);
                                            }, 200);
                                        }
                                        else
                                        {
                                            if (this.emails.includes(this.emailCad))
                                            {
                                                setTimeout(() => {
                                                    this.sucesso = null;
                                                    this.error = "Email já utilizado";
                                                        
                                                    setTimeout(() => {
                                                        this.mostrar_entrada = true;
                                                    }, 1200);
                                                }, 200);
                                            }
                                            else
                                            {
                                                if (!this.emailCad.includes("@gmail.com") && !this.emailCad.includes("@hotmail.com"))
                                                {
                                                    setTimeout(() => {
                                                        this.sucesso = null;
                                                        this.error = "O email deve conter @gmail ou @hotmail";
                                                                    
                                                        setTimeout(() => {
                                                            this.mostrar_entrada = true;
                                                        }, 1200);
                                                    }, 200);
                                                }
                                                else
                                                {
                                                    if (!(this.passwordCad && this.passwordCad === this.passwordCad2))
                                                    {
                                                        setTimeout(() => {
                                                            this.sucesso = null;
                                                            this.error = "Senha Incorreta!!!";
                                                                
                                                            setTimeout(() => {
                                                                this.mostrar_entrada = true;
                                                            }, 1200);
                                                        }, 200);
                                                    }
                                                    else
                                                    {
                                                        if (this.passwordCad.includes(" "))
                                                        {
                                                            setTimeout(() => {
                                                                this.sucesso = null;
                                                                this.error = "A senha não pode conter espaço🚫";
                                                                    
                                                                setTimeout(() => {
                                                                    this.mostrar_entrada = true;
                                                                }, 1200);
                                                            }, 200);
                                                        }
                                                        else
                                                        {
                                                            if (!(this.passwordCad.length > 5))
                                                            {
                                                                setTimeout(() => {
                                                                    this.sucesso = null;
                                                                    this.error = "A senha deve conter pelo menos 6 caracteres";
                                                                        
                                                                    setTimeout(() => {
                                                                        this.mostrar_entrada = true;
                                                                    }, 1200);
                                                                }, 200);
                                                            }
                                                            else
                                                            {
                                                                this.names.push(this.usernameCad);
                                                                this.emails.push(this.emailCad);
                                                                this.senhas.push(this.passwordCad);
                            
                                                                localStorage.setItem("names", JSON.stringify(this.names));
                                                                localStorage.setItem("emails", JSON.stringify(this.emails));
                                                                localStorage.setItem("senhas", JSON.stringify(this.senhas));
                            
                                                                setTimeout(() => 
                                                                {
                                                                    this.error = null;
                                                                    this.sucesso = "🌟Conta Criada🌟";
                                                        
                                                                    setTimeout(() => {
                                                                        this.mostrar_entrada = true;
                                                        
                                                                        setTimeout(() => {
                                                                            this.sucesso = null;
                                                                            this.error = null;
                                                                        }, 1200);
                                                                    }, 1200);
                                                                }, 200);

                                                                setTimeout(() => {
                                                                    this.usernameCad = "";
                                                                    this.emailCad = "";
                                                                    this.passwordCad = "";
                                                                    this.passwordCad2 = "";
                                                                    this.fazer_login = true;
                                                                    this.criar_conta = false;
                                                                }, 1500);

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }, 300);
                }, 200);
            }, 200);
        },

        logoff()
        {
            if(confirm("Tem certeza que deseja sair desta conta?"))
            {
                this.logado = false;
            }
        },

        ver_cadastrados()
        {
            this.mostrar_lista =! this.mostrar_lista;
                
            if (localStorage.getItem("names") && localStorage.getItem("emails"))
            {
                this.names = JSON.parse(localStorage.getItem("names"));
                this.emails = JSON.parse(localStorage.getItem("emails"));
            }
            
        },

        checkout()
        {
            
        }
    },

}).mount("#app");