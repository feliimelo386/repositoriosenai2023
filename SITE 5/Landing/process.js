const {createApp} = Vue;

createApp({
    data(){
        return{
            products: [
                {
                    id: 1,
                    name: "Tênis",
                    description: "O melhor tênis de corrida legalzão",
                    price: 107.99,
                    image: "./imagens/tenis.webp"
                },

                {
                    id: 2,
                    name: "Botas",
                    description: "Botas para anões",
                    price: 149.99,
                    image: "./imagens/botas.webp"
                },

                {
                    id: 3,
                    name: "Sapatos",
                    description: "Sapatos para pessoas de negócios",
                    price: 112.49,
                    image: "./imagens/sapatos.webp"
                },

                {
                    id: 4,
                    name: "Sandálias",
                    description: "Sandálias romanas bem loko",
                    price: 59.99,
                    image: "./imagens/sandalias.webp"
                },
            ],

            currentProduct: {},
            cart: [],
        };
    },

    mounted(){
        window.addEventListener("hashchange", this.updateProduct);
        this.updateProduct();
    },

    //Função VUE para retornar resultados especificos de um bloco programado
    computed:{
        cartItemCount()
        {
            return this.cart.length;
        },

        cartTotal()
        {
            return this.cart.reduce((total, product) => total + product.price, 0);
        },
    },
    
    methods:{
        updateProduct()
        {
            const productId = window.location.hash.split("/")[2];
            const product = this.products.find(item => item.id.toString() === productId);
            this.currentProduct = product ? product : {};
        },

        addToCart(product)
        {
            this.cart.push(product);
        },

        removeFroCart(product)
        {
            const index = this.cart.indexOf(product);

            if (index != -1)
            {
                this.cart.splice(index, 1);
            }
        },
    },
}).mount("#app");